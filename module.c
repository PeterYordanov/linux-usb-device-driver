#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/usb.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Peter Yordanov");
MODULE_DESCRIPTION("A simple USB device driver");
MODULE_VERSION("0.1");

#define USB_VENDOR_ID  (0x22d9)
#define USB_PRODUCT_ID (0x2764) 

static int __init etx_usb_init(void)
{
    return usb_register(&etx_usb_driver);
}

static void __exit etx_usb_exit(void)
{
    usb_deregister(&etx_usb_driver);
}

static int etx_usb_probe(struct usb_interface* interface, const struct usb_device_id* id)
{
    unsigned int i;
    unsigned int endpoints_count;
    struct usb_host_interface* iface_desc = interface->cur_altsetting;

    dev_info(&interface->dev, "USB Driver Probed: Vendor ID : 0x%02x,\t"
             "Product ID : 0x%02x\n", id->idVendor, id->idProduct);
             
    endpoints_count = iface_desc->desc.bNumEndpoints;
    
	pr_info("USB_INTERFACE_DESCRIPTOR:\n");                      
    pr_info("-----------------------------\n");                  
    pr_info("Length: 0x%x\n", iface_desc->desc.bLength);                       
    pr_info("DescriptorType: 0x%x\n", iface_desc->desc.bDescriptorType);       
    pr_info("InterfaceNumber: 0x%x\n", iface_desc->desc.bInterfaceNumber);     
    pr_info("AlternateSetting: 0x%x\n", iface_desc->desc.bAlternateSetting);   
    pr_info("NumEndpoints: 0x%x\n", iface_desc->desc.bNumEndpoints);           
    pr_info("InterfaceClass: 0x%x\n", iface_desc->desc.bInterfaceClass);       
    pr_info("InterfaceSubClass: 0x%x\n", iface_desc->desc.bInterfaceSubClass); 
    pr_info("InterfaceProtocol: 0x%x\n", iface_desc->desc.bInterfaceProtocol); 
    pr_info("Interface: 0x%x\n", iface_desc->desc.iInterface);                 
    pr_info("-----------------------------\n");
    
    for (i = 0; i < endpoints_count; ++i) {
		pr_info("USB_ENDPOINT_DESCRIPTOR:\n");                  
		pr_info("------------------------\n");                  
		pr_info("bLength: 0x%x\n", iface_desc->endpoint[i].desc.bLength);                  
		pr_info("bDescriptorType: 0x%x\n", iface_desc->endpoint[i].desc.bDescriptorType);  
		pr_info("bEndPointAddress: 0x%x\n", iface_desc->endpoint[i].desc.bEndpointAddress);
		pr_info("bmAttributes: 0x%x\n", iface_desc->endpoint[i].desc.bmAttributes);        
		pr_info("wMaxPacketSize: 0x%x\n", iface_desc->endpoint[i].desc.wMaxPacketSize);    
		pr_info("bInterval: 0x%x\n", iface_desc->endpoint[i].desc.bInterval);              
		pr_info("\n");                                          
		pr_info("------------------------\n");                  
	}
	
	return 0;
}

static void etx_usb_disconnect(struct usb_interface* interface)
{
    dev_info(&interface->dev, "USB Driver Disconnected\n");
}

const struct usb_device_id etx_usb_table[] = {
    { USB_DEVICE(USB_VENDOR_ID, USB_PRODUCT_ID) },
    { }
};

MODULE_DEVICE_TABLE(usb, etx_usb_table);

static struct usb_driver etx_usb_driver = {
    .name       = "USB Driver",
    .probe      = etx_usb_probe,
    .disconnect = etx_usb_disconnect,
    .id_table   = etx_usb_table,
};

module_init(etx_usb_init);
module_exit(etx_usb_exit);
